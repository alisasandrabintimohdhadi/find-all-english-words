package com.gitlab.epadronu.findallenglishwords.aspects;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class EnglishWordsFinderFlowTracingAspect extends FlowTracingAspect {

  @Override
  @Pointcut("initialization(com.gitlab.epadronu.findallenglishwords.EnglishWordsFinder+.new(..))")
  protected void constructors() {
  }

  @Override
  @Pointcut(
   "execution(* com.gitlab.epadronu.findallenglishwords.EnglishWordsFinder+.findAllWordsIn(..))")
  protected void methods() {
  }
}
