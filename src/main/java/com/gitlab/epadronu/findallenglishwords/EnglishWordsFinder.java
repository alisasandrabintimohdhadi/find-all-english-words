package com.gitlab.epadronu.findallenglishwords;

import java.net.ConnectException;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.github.dakusui.combinatoradix.Permutator;

/**
 * Find all English words that can be found in all possible permutations of a given string.
 */
public final class EnglishWordsFinder {

  @NotNull
  @Valid
  private final OnlineDictionary dictionary;

  /**
   * Create a new instance with the provided dictionary implementation.
   *
   * @param dictionary the implementation to be used for performing the word look ups
   */
  public EnglishWordsFinder(@NotNull @Valid final OnlineDictionary dictionary) {
    this.dictionary = dictionary;
  }

  /**
   * Find all English words that can be found in all possible permutations of a given string.
   * <p>
   * If the provided string is null or is blank, then an empty set is returned.
   * <p>
   * All not printable characters are ignored.
   *
   * @param string the string to be used for finding out all possible English words
   *
   * @return a collection with all the found words
   */
  @NotNull
  public Set<@NotEmpty String> findAllWordsIn(final String string) {
    try {
      if (dictionary.getKnownWords().isEmpty()) {
        return Collections.emptySet();
      }
    } catch (ConnectException ignore) {
    }

    if (string == null) {
      return Collections.emptySet();
    }

    final String cleanedString = string.replaceAll("\\s+", "");

    if (cleanedString.isEmpty()) {
      return Collections.emptySet();
    }

    return getAllPossibleAndUniquePermutations(cleanedString)
     .stream()
     .filter(dictionary::isEnglishWord)
     .collect(Collectors.toSet());
  }

  /**
   * Get all possible permutation for the given string.
   * <p>
   * For example, giving the string ABC, we will obtain: [A, B, C, AB, BA, AC, CA, BC, CB, ABC, CBA,
   * BAC, CAB, ACB, BCA]
   *
   * @param string the string to be permuted
   *
   * @return all possible permutation for the given string
   */
  private static Set<String> getAllPossibleAndUniquePermutations(final String string) {
    final Set<String> combinations = new HashSet<>();

    final List<Character> characters = string
     .chars()
     .mapToObj(it -> (char) it)
     .collect(Collectors.toList());

    for (int k = 1; k <= string.length(); k++) {
      for (List<Character> permutation : new Permutator<>(characters, k)) {
        combinations.add(permutation.stream().map(String::valueOf).collect(Collectors.joining()));
      }
    }

    return combinations;
  }
}
