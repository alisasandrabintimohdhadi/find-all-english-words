package com.gitlab.epadronu.findallenglishwords;

import java.net.ConnectException;
import java.util.Set;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * Describe the contract any online dictionary implementation must comply to.
 * <p>
 * Each implementation is responsible of defining the mechanism thorough which the collection of
 * known words will be retrieved.
 */
@FunctionalInterface
public interface OnlineDictionary {

  /**
   * Perform a look up for the given word among this dictionary's known words.
   * <p>
   * If there's an issue retrieving the collection of known words, an IllegalStateException will be
   * thrown since the look up cannot be performed.
   *
   * @param word a non-empty (null or blank) string to be validated as an English word
   *
   * @return true if the given word is part of the English language, false otherwise
   */
  default boolean isEnglishWord(@NotEmpty final String word) {
    final Set<String> knownWords;

    try {
      knownWords = getKnownWords();
    } catch (ConnectException e) {
      throw new IllegalStateException(
       "Something went wrong when trying to get the collection of known words", e);
    }

    return knownWords.contains(word.toLowerCase());
  }

  /**
   * Retrieve this dictionary's collection of known words.
   * <p>
   * IMPORTANT: All words must be in lowercase.
   *
   * @return a collection of words known to this dictionary
   *
   * @throws ConnectException in case the retrieval cannot be performed
   */
  @NotNull
  Set<@NotEmpty String> getKnownWords() throws ConnectException;
}
